import * as React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

class SubExtDocument extends Document {
  render(): JSX.Element {
    return (
      <Html>
        <Head>
          <script src="https://extension-files.twitch.tv/helper/v1/twitch-ext.min.js" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default SubExtDocument;
