import * as React from 'react';
import root from 'window-or-global';

type Theme = 'light' | 'dark' | undefined;

const Layout: React.FC = () => {
  const [theme, setTheme] = React.useState<Theme>('light');
  const [isVisible, setIsVisible] = React.useState(true);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    if (root.Twitch) {
      console.log(root.Twitch);
      root.Twitch.ext.onAuthorized(async (auth) => {
        console.debug(`auth token: ${auth}`);
        setLoading(false);
      });

      root.Twitch.ext.listen('broadcast', (target, contentType, body) => {
        console.debug(
          `Message received for ${target} (${contentType}): ${body}`
        );
      });

      root.Twitch.ext.onVisibilityChanged((newIsVisible: boolean) => {
        setIsVisible(newIsVisible);
      });

      root.Twitch.ext.onContext((context, delta) => {
        if (delta.includes('theme')) {
          setTheme(context.theme);
        }
      });
    }

    return () => {
      if (!root.Twitch) {
        return;
      }

      root.Twitch.ext.unlisten('broadcast', () => {
        console.debug('Successfully unlistened');
      });
    };
  }, [root.Twitch]);

  return loading || !isVisible ? null : <p>{theme}</p>;
};

export default Layout;
