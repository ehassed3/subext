<table>
    <tr>
        <td><b>Возможность</b></td>
        <td><b>Откуда</b></td>
        <td><b>Описание</b></td>
        <td><b>Ссылка на доку</b></td>
    </tr>
    <tr>
        <td>Создать секретный токен (расширения?)</td>
        <td>Запрос в API</td>
        <td>Создает секретный токен</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#create-extension-secret">ссылка</a></td>
    </tr>
    <tr>
        <td>Получить секретный токен (расширения?)</td>
        <td>Запрос в API</td>
        <td>Возвращает секретный токен</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#get-extension-secret">ссылка</a></td>
    </tr>
    <tr>
        <td>Немедленно удалить все данные о секретных токенах</td>
        <td>Запрос в API</td>
        <td>Удаляет все данные о секретных токенах, при этом все перестанет работать до создания нового</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#revoke-extension-secrets">ссылка</a></td>
    </tr>
    <tr>
        <td>Получить данные о каналах с активированным расширением</td>
        <td>Запрос в API</td>
        <td>Возвращает массив данных о live каналах, где включено расширение</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#get-live-channels-with-extension-activated">ссылка</a></td>
    </tr>
    <tr>
        <td>Установить обязательную конфигурацию расширения</td>
        <td>Запрос в API</td>
        <td>Пока не очень понятно, как это работает</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#set-extension-required-configuration">ссылка</a></td>
    </tr>
    <tr>
        <td>Установить конфигурацию расширения для отдельного сегмента</td>
        <td>Запрос в API</td>
        <td>
            Возможность менять конфигурацию для различных типов пользователей("global", "developer", "broadcaster")<br>
            Пока не очень понятно, как это работает
        </td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#set-extension-configuration-segment">ссылка</a></td>
    </tr>
    <tr>
        <td>Получить конфигурацию расширения</td>
        <td>Запрос в API</td>
        <td>Получить конфигурацию расширения</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#get-extension-channel-configuration">ссылка</a></td>
    </tr>
    <tr>
        <td>Получить конфигурацию для отдельного сегмента</td>
        <td>Запрос в API</td>
        <td>Получить конфигурацию расширения какого-то типа пользователей ("global", "developer", "broadcaster")</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#get-extension-configuration-segment">ссылка</a></td>
    </tr>
    <tr>
        <td>Отправить сообщение с данными (не чат!) всем каналам или отдельному каналу</td>
        <td>Запрос в API</td>
        <td>
            Отправить сообщение с данными (не чат!) отдельному каналу или всем каналам, на которых активировано расширение.<br>
            Делается с помощью <a href="https://dev.twitch.tv/docs/pubsub">PubSub</a>
        </td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#send-extension-pubsub-message">ссылка</a></td>
    </tr>
    <tr>
        <td>Отправить сообщение в чат канала</td>
        <td>Запрос в API</td>
        <td>Отправить тектовое сообщение в чат канала</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#send-extension-chat-message">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие на авторизацию пользователя</td>
        <td>Twitch.ext.onAuthorized</td>
        <td>Действие, которое произойдет при авторизации пользователя</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие на изменение контекста расширения</td>
        <td>Twitch.ext.onContext</td>
        <td>Действие, которое произойдет при изменении контекста расширения</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие на ошибку расширения</td>
        <td>Twitch.ext.onError</td>
        <td>Действие, которое произойдет при возникновении ошибки в расширении</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие, которое произойдет при ховере на расширение</td>
        <td>Twitch.ext.onHighlightChanged</td>
        <td>Действие, которое произойдет при ховере на расширение. Работает только для расширений поверх видео</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие, которое произойдет при изменении позиции расширения</td>
        <td>Twitch.ext.onPositionChanged</td>
        <td>Действие, которое произойдет при изменении позиции расширения. Работает только для расширений поверх видео</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие, которое произойдет при изменении видимости расширения</td>
        <td>Twitch.ext.onVisibilityChanged</td>
        <td>Действие, которое произойдет при изменении видимости расширения</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Отправка сообщения (не чат!) стримеру или пользователю</td>
        <td>Twitch.ext.send</td>
        <td>Отправка сообщения (не чат!) стримеру или пользователю с помощью <a href="https://dev.twitch.tv/docs/pubsub">PubSub</a></td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Подписаться на получение сообщений от Twitch.ext.send</td>
        <td>Twitch.ext.listen</td>
        <td>Позволяет подписаться на получение сообщений (не чат!) из <a href="https://dev.twitch.tv/docs/pubsub">PubSub</a></td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Отписаться от Twitch.ext.listen</td>
        <td>Twitch.ext.unlisten</td>
        <td>Позволяет отписаться от Twitch.ext.listen</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-extensions">ссылка</a></td>
    </tr>
    <tr>
        <td>Подписаться на канал</td>
        <td>Twitch.ext.actions.followChannel</td>
        <td>Подписаться на любой канал на Twitch</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-actions">ссылка</a></td>
    </tr>
    <tr>
        <td>Минимизировать расширение</td>
        <td>Twitch.ext.actions.minimize</td>
        <td>Минимизировать видео-расширение</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-actions">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие, которое произойдет при follow'е на канал</td>
        <td>Twitch.ext.actions.onFollow</td>
        <td>Действие, которое произойдет при follow'е на отслеживаемый канал</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-actions">ссылка</a></td>
    </tr>
    <tr>
        <td><i><b>Запрос на получение id пользователя</b></i></td>
        <td>Twitch.ext.actions.requestIdShare</td>
        <td>Запрос на получение id пользователя, если он согласится, то сработает функция переданная в Twitch.ext.onAuthorized</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-actions">ссылка</a></td>
    </tr>
    <tr>
        <td>Получить конфигурацию настроек стримера</td>
        <td>Twitch.ext.configuration.broadcaster</td>
        <td>Получить конфигурацию настроек стримера</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-configuration">ссылка</a></td>
    </tr>
    <tr>
        <td>Получить конфигурацию настроек разработчика</td>
        <td>Twitch.ext.configuration.developer</td>
        <td>Получить конфигурацию настроек разработчика</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-configuration">ссылка</a></td>
    </tr>
    <tr>
        <td>Получить конфигурацию настроек расширения</td>
        <td>Twitch.ext.configuration.global</td>
        <td>Получить конфигурацию настроек</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-configuration">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие, которое произойдет после изменения конфигурации</td>
        <td>Twitch.ext.configuration.onChange</td>
        <td>Действие, которое произойдет после изменения конфигурации</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-configuration">ссылка</a></td>
    </tr>
    <tr>
        <td>Установить конфигурацию настроек</td>
        <td>Twitch.ext.configuration.set</td>
        <td>Установить конфигурацию настроек</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-configuration">ссылка</a></td>
    </tr>
    <tr>
        <td>Активированы ли Bits в расширении</td>
        <td>Twitch.ext.features.isBitsEnabled</td>
        <td>Активированы ли Bits в расширении</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-feature-flags">ссылка</a></td>
    </tr>
    <tr>
        <td>Активирован ли чат в расширении</td>
        <td>Twitch.ext.features.isChatEnabled</td>
        <td>Активирован ли чат в расширении</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-feature-flags">ссылка</a></td>
    </tr>
    <tr>
        <td>Активирована ли возможность просмотра статуса подписки в расширении</td>
        <td>Twitch.ext.features.isSubscriptionStatusAvailable</td>
        <td>Активирована ли возможность просмотра статуса подписки в расширении</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-feature-flags">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие, которое произойдет при изменении одного из статусов</td>
        <td>Twitch.ext.features.onChange</td>
        <td>Действие, которое произойдет при изменении одного из статусов%
            <ul>
                <li>активированности Bits в расширении</li>
                <li>активированности чата в расширении</li>
                <li>активированности просмотра статуса подписки в расширении</li>
            </ul>
        </td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-feature-flags">ссылка</a></td>
    </tr>
    <tr>
        <td>Получить список услуг за Bits</td>
        <td>Twitch.ext.bits.getProducts</td>
        <td>Получить список услуг за Bits</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-bits">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие, которое произойдет после окончания транзакции</td>
        <td>Twitch.ext.bits.onTransactionCancelled</td>
        <td>Действие, которое произойдет после окончания транзакции</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-bits">ссылка</a></td>
    </tr>
    <tr>
        <td>Сымитировать покупку за Bits</td>
        <td>Twitch.ext.bits.setUseLoopBack</td>
        <td>Имитация покупки за Bits, к примеру если услуга досталась в награду</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-bits">ссылка</a></td>
    </tr>
    <tr>
        <td>Показать баланс Bits пользователя</td>
        <td>Twitch.ext.bits.showBitsBalance</td>
        <td>Показать баланс Bits пользователя</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-bits">ссылка</a></td>
    </tr>
    <tr>
        <td>Купить услугу за Bits</td>
        <td>Twitch.ext.bits.useBits</td>
        <td>Купить услугу за Bits</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-bits">ссылка</a></td>
    </tr>
    <tr>
        <td><i><b>Получить opaqueId пользователя</b></i></td>
        <td>Twitch.ext.viewer.opaqueId</td>
        <td>Получить opaqueId пользователя</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-viewer">ссылка</a></td>
    </tr>
    <tr>
        <td><i><b>Получить id пользователя</b></i></td>
        <td>Twitch.ext.viewer.id</td>
        <td>Получить id залогиненного пользователя (если он давал согласие)</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-viewer">ссылка</a></td>
    </tr>
    <tr>
        <td><i><b>Получить роль пользователя</b></i></td>
        <td>Twitch.ext.viewer.role</td>
        <td>Получить роль пользователя</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-viewer">ссылка</a></td>
    </tr>
    <tr>
        <td><i><b>Поделился ли пользователь своим id с расширением</b></i></td>
        <td>Twitch.ext.viewer.isLinked</td>
        <td>Поделился ли пользователь своим id с расширением</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-viewer">ссылка</a></td>
    </tr>
    <tr>
        <td><i><b>Получить токен пользователя</b></i></td>
        <td>Twitch.ext.viewer.sessionToken</td>
        <td>Получить токен пользователя</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-viewer">ссылка</a></td>
    </tr>
    <tr>
        <td><i><b>Получить статус подписки пользователя</b></i></td>
        <td>Twitch.ext.viewer.subscriptionStatus</td>
        <td>Получить статус подписки пользователя</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-viewer">ссылка</a></td>
    </tr>
    <tr>
        <td>Действие, которое произойдет при изменении статуса пользователя</td>
        <td>Twitch.ext.viewer.onChange</td>
        <td>Действие, которое произойдет при изменении статуса пользователя</td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#helper-viewer">ссылка</a></td>
    </tr>
    <tr>
        <td>Доп. данные о контексте расширения</td>
        <td></td>
        <td>
            Доп. данные о:
            <ul>
                <li>О виде расширения</li>
                <li>О языке пользователя</li>
                <li>О локали пользователя ("en-US"/"en-UK")</li>
                <li>О моде расширения ("config", "dashboard", "viewer")</li>
                <li>О платформе ("mobile", "web")</li>
                <li>В попапе расширение или нет</li>
                <li>О статусе расширения ("testing", "hosted_test", "approved", "released", "ready_for_review", "in_review", "pending_action", "uploading")</li>
            </ul>
        </td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#client-query-parameters">ссылка</a></td>
    </tr>
    <tr>
        <td>Вид ответа JWT</td>
        <td></td>
        <td></td>
        <td><a href="https://dev.twitch.tv/docs/extensions/reference#jwt-schema">ссылка</a></td>
    </tr>
</table>
